const filter = require('./everyOther');
const assert = require('assert');

describe('Every other filter', () => {
  it('removes every other element of an array', () => {
    const inputData = [1, 2, 3, 4, 5, 6];
    const expectedResult = [1, 3, 5];

    assert.deepEqual(filter(inputData), expectedResult);
  });
});
