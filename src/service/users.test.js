const service = require('./users');
const assert = require('assert');

const mockFetcher = async () => {
  mockFetcher.wasCalled = true;
  return 'fetched!';
};

const mockBuilder = () => {
  mockBuilder.wasCalled = true;
  return 'built!';
};

const mockParser = () => {
  mockParser.wasCalled = true;
  return 'parsed!';
};

const mockFilter = () => {
  mockFilter.wasCalled = true;
  return 'filtered!';
};

const mockConverter = () => {
  mockConverter.wasCalled = true;
  return 'converted!';
};

const serviceArguments = [mockFetcher, mockBuilder, mockParser, mockFilter, mockConverter];

describe.only('Users service', () => {
  beforeEach(() => {
    mockFetcher.wasCalled = false;
    mockBuilder.wasCalled = false;
    mockParser.wasCalled = false;
    mockFilter.wasCalled = false;
    mockConverter.wasCalled = false;
  });

  it('calls fetcher function', async () => {
    assert.equal(mockFetcher.wasCalled, false);
    await service.apply(this, serviceArguments);
    assert.equal(mockFetcher.wasCalled, true);
  });

  it('calls builder function', async () => {
    assert.equal(mockBuilder.wasCalled, false);
    await service.apply(this, serviceArguments);
    assert.equal(mockBuilder.wasCalled, true);
  });

  it('calls parser function', async () => {
    assert.equal(mockParser.wasCalled, false);
    await service.apply(this, serviceArguments);
    assert.equal(mockParser.wasCalled, true);
  });

  it('calls filter function', async () => {
    assert.equal(mockFilter.wasCalled, false);
    await service.apply(this, serviceArguments);
    assert.equal(mockFilter.wasCalled, true);
  });

  it('calls converter function', async () => {
    assert.equal(mockConverter.wasCalled, false);
    await service.apply(this, serviceArguments);
    assert.equal(mockConverter.wasCalled, true);
  });
});
