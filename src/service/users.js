const urlFetcher = require('../fetcher/fetcher');
const urlBuilder = require('../builder/url');
const jsonParser = require('../parser/json');
const everyOtherFilter = require('../filters/everyOther');
const csvConverter = require('../converter/csv');

module.exports = async (
  fetcher = urlFetcher,
  builder = urlBuilder,
  parser = jsonParser,
  filter = everyOtherFilter,
  converter = csvConverter,
) => {
  const fetchedData = await fetcher(builder('json'));
  const parsedData = parser(fetchedData);
  const filteredData = filter(parsedData);
  const convertedData = converter(filteredData);

  return convertedData;
};

