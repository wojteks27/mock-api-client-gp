require('isomorphic-fetch');

module.exports = async (url, fetcher = fetch) => {
  const response = await fetcher(url);
  return response.text();
};
