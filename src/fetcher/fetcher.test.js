const fetcher = require('./fetcher');
const assert = require('assert');

// object satisfying minimal interface of Fetch
const mockedFetcherFunction = async () => ({
  text: async () => 'data was fetched',
});

describe('Async API fetcher', () => {
  it('calls fetcher function to obtain data', async () => {
    const fetchedData = await fetcher('any-url', mockedFetcherFunction);

    assert.deepEqual(fetchedData, 'data was fetched');
  });
});
