const parse = require('./json');
const assert = require('assert');

// object satisfying minimal interface of JSON
const mockedParserObject = {
  parse: () => 'was called',
};

describe('JSON to JS object parsing service', () => {
  it('calls parser function', () => {
    const callResult = parse('[1,2,3]', mockedParserObject);

    assert.deepEqual(callResult, 'was called');
  });
});
