const jsonParser = JSON;

module.exports = (data, parser = jsonParser) => parser.parse(data);
