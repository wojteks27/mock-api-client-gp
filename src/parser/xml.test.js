const parse = require('./json');
const assert = require('assert');

// object satisfying minimal interface of pixl-xml library
const mockedParserObject = {
  parse: () => ({
    person: 'was called',
  }),
};

describe('XML to JS object parsing service', () => {
  it('calls parser function', () => {
    const callResult = parse('[1,2,3]', mockedParserObject);

    assert.deepEqual(callResult.person, 'was called');
  });
});
