const XML = require('pixl-xml');

module.exports = (data, parser = XML) => parser.parse(data).person;
