const convert = require('./htmlTable');
const assert = require('assert');

// any function is ok here (why?)
const mockedConvertFunction = () => 'was called';

describe('JS Object to HTML table converting service', () => {
  it('calls converter function', () => {
    const callResult = convert([1, 2, 3], mockedConvertFunction);

    assert.deepEqual(callResult, 'was called');
  });
});
