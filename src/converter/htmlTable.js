const tableify = require('tableify');

module.exports = (data, converter = tableify) => converter(data);
