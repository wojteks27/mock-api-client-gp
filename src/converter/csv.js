const CSV = require('csv-string');

module.exports = (data, converter = CSV) => {
  let result = '';

  data.forEach((item) => {
    result += converter.stringify(item);
  });

  return result;
};
