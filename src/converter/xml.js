const js2xmlparser = require('js2xmlparser');

module.exports = (data, converter = js2xmlparser) => converter.parse('person', data);
