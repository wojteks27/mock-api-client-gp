const convert = require('./csv');
const assert = require('assert');

// object satisfying minimal interface of CSV library
const mockedConverterObject = {
  stringify: () => 'was called',
};

describe('JS Object to CSV converting service', () => {
  it('iterates over array and converts objects', () => {
    const callResult = convert([1, 2, 3], mockedConverterObject);

    assert.equal(callResult, 'was calledwas calledwas called');
  });
});
