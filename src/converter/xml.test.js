const convert = require('./xml');
const assert = require('assert');

// object satisfying minimal interface of js2xmlparser
const mockedConverterObject = {
  parse: () => 'was called',
};

describe('JS Object to XML converting service', () => {
  it('calls converter function', () => {
    const callResult = convert([1, 2, 3], mockedConverterObject);

    assert.deepEqual(callResult, 'was called');
  });
});
