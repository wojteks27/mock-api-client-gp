const builder = require('./url');
const assert = require('assert');

describe('URL building service', () => {
  it('builds url based on format parameter', () => {
    assert.equal(builder('json'), 'http://localhost:3030/api/users.json');
  });

  it('throws exception when format not allowed', () => {
    assert.throws(() => {
      builder('not-allowed');
    }, Error);
  });
});
