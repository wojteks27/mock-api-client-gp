const API_HOST = process.env.API_HOST || 'http://localhost:3030';
const SUPPORTED_FORMATS = ['json', 'csv', 'xml'];

module.exports = (format) => {
  if (SUPPORTED_FORMATS.indexOf(format) === -1) {
    throw new Error(`Unsupported data format ${format}!`);
  }

  return `${API_HOST}/api/users.${format}`;
};
