const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;

const usersService = require('./src/service/users');

app.get('/', async (req, res, next) => {
  try {
    res.send(await usersService());
  } catch (e) {
    next(e);
  }
});

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));
