# mock-api-client-gp   

## What?

The task is preparation of a node.js app which will consume an API, process the data and output it.

## What API?

[https://bitbucket.org/wojteks27/mock-api-gp](https://bitbucket.org/wojteks27/mock-api-gp)

The repo is public, no need to ask for access.

Follow `README.md` to install the API locally.

## What should you do?

* fetch users data from `mock-api-gp` using endpoint `/api/users.json`
* process data in a following way (it should be processed in only one of those fashions at a time, it should be inter-changeable with one line of code)
    * extract only names starting with character 'a' (non-case-sensitive)
    * extract every other record in a simplest possible way
* print processed data as a HTML table

## How should you do it?

* in compliance with the best practices of software engineering
* remember, everything can change ]:->

# Step two (when you are done with the first implementation)

* fetch users data from `mock-api-gp` using endpoint `/api/users.xml`
* print processed data as CSV string

Please implement in way that will allow switching back to previous requirements as simple as possible!
